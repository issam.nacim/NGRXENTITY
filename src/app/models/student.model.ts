/**
 * Created by issam on 09/12/2017.
 */
export  interface Student {
    id: number;
    name: string;
    smart: string;
    age: number;
}
