import { NgModule } from '@angular/core';
import { StudentComponent } from '../component/student/student.component';

import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { studentReducer } from './student/student.reducer';

@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature('student', studentReducer)
  ],
  exports: [StudentComponent],
  declarations: [StudentComponent]
})
export class StoreAppModule { }
