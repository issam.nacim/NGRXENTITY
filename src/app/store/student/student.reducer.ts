import * as actions from './student.actions';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
import {Student} from '../../models/student.model';

// Main data interface



// Entity adapter
export const studentAdapter = createEntityAdapter<Student>();
export interface State extends EntityState<Student> { }


export const initialState: State = studentAdapter.getInitialState(null);

// Reducer

export function studentReducer  (state: State = initialState, action: actions.StudentActions) {

    switch (action.type) {
      case actions.ActionTypes.CREATE: {
        return studentAdapter.addOne(action.playload, state);
      }
      case actions.ActionTypes.UPDATE:
            return studentAdapter.updateOne(action.playload, state);
      case actions.ActionTypes.DELETE:
            return studentAdapter.removeOne(action.playload, state);
      default:
            return state;
        }
}

// Create the default selectors

export const getStudentState = createFeatureSelector<State>('student');

export const {selectIds, selectEntities, selectAll, selectTotal} = studentAdapter.getSelectors(getStudentState);



