import { Action } from '@ngrx/store';
import {type} from '../../utils/type';


export const ActionTypes = {
  CREATE: type('[Student] Create'),
  UPDATE: type('[Student] Update'),
  DELETE: type('[Student] Delete'),
};
export class Create implements Action {
    type = ActionTypes.CREATE;
    constructor(public playload: any) { }
}

export class Update implements Action {
    type = ActionTypes.UPDATE;
    constructor(public playload: any) { }
}

export class Delete implements Action {
    type = ActionTypes.DELETE;
    constructor(public playload: any) { }
}

export type StudentActions = Create | Update | Delete;
