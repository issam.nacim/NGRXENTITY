import { ActionReducerMap } from '@ngrx/store';
import { studentReducer } from './student/student.reducer';

export const reducers: ActionReducerMap<any> = {
  student: studentReducer
};
