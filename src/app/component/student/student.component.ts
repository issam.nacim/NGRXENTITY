import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import * as actions from '../../store/student/student.actions';
import * as fromStudent from '../../store/student/student.reducer';
import {Student} from '../../models/student.model';


@Component({
  selector: 'app-order',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.sass']
})
export class StudentComponent implements OnInit {

  students: Observable<any>;

  constructor(private store: Store<fromStudent.State>) { }

  ngOnInit() {
    this.students = this.store.select(fromStudent.selectAll);
  }

  createStudent() {
    const student: Student = {
      id: new Date().getUTCMilliseconds(),
      name: 'student',
      smart: 'smart',
      age: 20,
    };

    this.store.dispatch( new actions.Create(student) );
  }

  updateStudent(student: Student, smart) {
    student.smart = smart;
    this.store.dispatch( new actions.Update(student)  );
  }

  deleteStudent(id) {
    this.store.dispatch( new actions.Delete(id));
  }

}
